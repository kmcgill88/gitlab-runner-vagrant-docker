# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  required_plugins = %w( vagrant-vbguest vagrant-disksize )
  _retry = false
  required_plugins.each do |plugin|
      unless Vagrant.has_plugin? plugin
          system "vagrant plugin install #{plugin}"
          _retry=true
      end
  end

  if (_retry)
      exec "vagrant " + ARGV.join(' ')
  end
  config.disksize.size = "50GB"

  config.vm.box = "ubuntu/bionic64"
  config.vm.hostname = "gitlab"
  config.vm.network "public_network", bridge: 'enp27s0'
  config.vm.synced_folder "~/vagrant/gitlab-runner", "/etc/gitlab-runner"
  config.vm.provider "virtualbox" do |vb|
    vb.gui = false  
    vb.memory = "4096"
    vb.cpus = 4
  end
  config.vm.provision "shell", inline: <<-SHELL
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash

sudo apt-get update

sudo apt-get install \
gitlab-runner \
apt-transport-https \
ca-certificates \
curl \
gnupg-agent \
software-properties-common -y

sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
"deb [arch=amd64] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) \
stable"

sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io -y

sudo docker run hello-world

sudo usermod -aG docker vagrant

sudo gitlab-runner register \
--non-interactive \
--url "https://gitlab.com/" \
--registration-token "YOUR_TOKEN" \
--executor "docker" \
--docker-image alpine:latest \
--description "gitlab-runner" \
--tag-list "docker" \
--run-untagged="true" \
--locked="false"

sudo gitlab-runner start
  SHELL
end
